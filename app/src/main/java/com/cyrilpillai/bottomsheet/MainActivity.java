package com.cyrilpillai.bottomsheet;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    MainBottomSheet mainBottomSheet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainBottomSheet = MainBottomSheet.newInstance();

        AppCompatButton btnShowDialog = (AppCompatButton) findViewById(R.id.btn_show_dialog);
        btnShowDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainBottomSheet.setCancelable(false);
                mainBottomSheet.show(getSupportFragmentManager(), "BottomSheet");
            }
        });

    }
}
